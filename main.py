
import telebot

from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup
)


TOKEN = '6830740737:AAGRKXqSigJyNsC0gFlrWEhwyEuqNdjrIrc'


bot = telebot.TeleBot(TOKEN)

users = {}


@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, """\
Hi there, I am EchoBot.
I am here to echo your kind words back to you. Just say anything nice and I'll say the exact same thing to you!\
""")
    

@bot.message_handler(commands=['help'])
def help_message_handler(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    keyboard.add('💸Прайс', '✅ В наличие', '🛍Магазин')
    keyboard.add('Как купить ⁉️ инструкция', '👉Пополнить btc/ltc')
    keyboard.add('👤Профиль/баланс', '🛎 Ненаход/Правила')
    keyboard.add('⭐️ ПРЕДЗАКАЗ ⭐️')
    bot.reply_to(message, "Вам надо идти налево", reply_markup=keyboard)

@bot.message_handler(commands=["save_me"])
def save_user_info(message):
    msg = bot.send_message(chat_id=message.chat.id, text="Отправьте свое имя")
    bot.register_next_step_handler(msg, save_name)    

@bot.message_handler(commands=["who_i"])
def get_user_info(message):
    user_data = users[message.chat.id]
    bot.send_message(chat_id=message.chat.id, text=f"Ваше имя {user_data['name']} {user_data['surname']}")    


@bot.message_handler(content_types=["text"])
def main_message_handler(message):
    if message.text == "💸Прайс":
        bot.send_message(chat_id=message.chat.id, text="Стоимость нашей услуги 10 000")
    elif message.text == "Как купить ⁉️ инструкция":
        bot.send_message(chat_id=message.chat.id, text="перейдите по ссылке https://www.youtube.com/")
    elif message.text == "✅ В наличие":
        bot.send_message(chat_id=message.chat.id, text="we have a lot of things such as cables and phone cases and electronics")  
    elif message.text == "🛍Магазин":
        bot.send_message(chat_id=message.chat.id, text="our shop is called Raida.shop")  
    elif message.text == "👤Профиль/баланс":
        bot.send_message(chat_id=message.chat.id, text="your profile must be updated ishak")   
    elif message.text == "🛎 Ненаход/Правила":
        bot.send_message(chat_id=message.chat.id, text="dont disrespect us")   
    elif message.text == "⭐️ ПРЕДЗАКАЗ ⭐️":
        markup = InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(
            InlineKeyboardButton("mbank", callback_data="cb_mbank"),
            InlineKeyboardButton("optima", callback_data="cb_optima"),
            InlineKeyboardButton(" o! - money", callback_data="cb_money")
            )  
        bot.send_message(chat_id=message.chat.id, text="вот способы оплаты", reply_markup=markup)  
    elif message.text == "👉Пополнить btc/ltc":
        markup = InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(
            InlineKeyboardButton("Yes", callback_data="cb_yes"),
            InlineKeyboardButton("No", callback_data="cb_no"),
            InlineKeyboardButton("Дай мне что-то", callback_data="cb_name")
            )
        bot.send_message(chat_id=message.chat.id, text="Это какой-то интересный текст", reply_markup=markup)
    else:
        bot.send_message(chat_id=message.chat.id, text="Такой команды у нас в программе не имеется!")    


@bot.message_handler(commands=["save_me"])
def save_user_info(message):
    msg = bot.send_message(chat_id=message.chat.id, text="Отправьте свое имя")
    bot.register_next_step_handler(msg, save_name)

@bot.message_handler(commands=["who_i"])
def get_user_info(message):
    user_data = users[message.chat.id]
    bot.send_message(chat_id=message.chat.id, text=f"Ваше имя {user_data['name']} {user_data['surname']}")


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    print(call.data)
    if call.data == "cb_yes":
        bot.send_message(chat_id=call.message.chat.id, text="Это ответ на 'yes'")
    elif call.data == "cb_no":
        bot.answer_callback_query(call.id, text="Это ответ на ваш отказ")
    elif call.data == "cb_name":
        bot.send_message(chat_id=call.message.chat.id, text="Это ваше Имя Фимилия")
    elif call.data == "cb_mbank":
        bot.send_message(chat_id=call.message.chat.id, text=" номер нашего мбанка - 0502511109") 
    elif call.data == "cb_optima":
        bot.send_message(chat_id=call.message.chat.id, text=" наша оптима - 4169585355504506")
    elif call.data == "cb_money":
        bot.send_message(chat_id=call.message.chat.id, text=" наши о!деньги - +996509511109")           

def save_name(message):
    users[message.chat.id] = {
        "name" : message.text
        }
    msg = bot.send_message(message.chat.id, text="Отправьте нам свою фамилию")
    bot.register_next_step_handler(msg, save_surname)

def save_surname(message):
    users[message.chat.id]["surname"] = message.text
    bot.send_message(chat_id=message.chat.id, text="Ваши данные мы сохранили...")


bot.infinity_polling()

